#!/bin/bash
set -e

if [[ $(echo "\c redmine; \dt" | PGPASSWORD=redmine psql -h $PG_SERVER -U redmine redmine) = *"No relations found"* ]]; then
   RAILS_ENV=production bundle exec rake db:migrate
   REDMINE_LANG=en RAILS_ENV=production bundle exec rake redmine:load_default_data
fi

exec "$@"